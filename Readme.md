# File-Change-Detector

Simple program which detects changes between two commits. You need java 8 runtime at this machine.

## Installation

Be sure you have java 8 on your machine and
set [JAVA_HOME](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/).

<b>Info:</b> The release page will no longer be maintained.

1. Download the distribution
   from https://gitlab.com/api/v4/projects/13312103/packages/generic/filechangedetector/<tagname like v1.3.5>
   /filechangedetector.zip) or browse at https://gitlab.com/ppiag/filechangedetector/-/packages.
2. Unzip the downloaded file, a new folder will be created. Goto the new directory.
3. [Configure](#Configuration) the system, you find an `filechangedetectorConfig.json.example` in the newly created
   folder.

## Configuration

You must rename `filechangedetectorConfig.json.example` (which is part of the distribution)
to `filechangedetectorConfig.json` and adjust the parameters.

If you need specific loggings, you can use the standard log4j2 mechanism.

### Configuration-parameters in filechangedetectorConfig.json.example

- gitRepo mandatory describes the path to the git repository.
- emailServer optional describes the config of an email-server. Obviously needed if you want to send E-Mails. I hope the
  parameters are self-explanatory. For Test you can drop it, then the message will be logged.
- zulipServer optional describes the config of an zulip-server. Obviously needed if you want to send to zulip. I hope
  the parameters are self-explanatory. For Test you can drop it, then the message will be logged.
  **WARNING** At the moment there is a problem sending messages with more than 1000 Characters, which leads to
  exceptions there fore the functionality is disabled.
- observerConfigs Mandatory list of arbitrary numbers of observer-configs. Described in the next section.

#### Observer-config

- subject is mandatory an defines the subject of e-mail or zulip-message.
- email an optional list of e-mail-adresses. The list can be empty.
- stream optional string which defines the zulip-stream
- branchPatterns a mandatory list of regular expessions to define which branches are observed.
- filePatterns a mandatory list of regular expessions to define which files are observed.

## Running

You can start the program with

* ` java -jar filechangedetector-*-spring-boot.jar run-information branchname startHash endHash potentialStartRefernce potentialStartHash2 ...`
  The List of potential target branches is optional. They are only used if the startHash isn't an anchestor of endHash.
  So if you don't have a startHash simply use 00000000 or any invalid hash. The program will determine the most recent
  common ancestors for the endHash and each of these potential start commits (via git merge-base). Those ancestors will
  then be sorted and the newest one will be used as startHash.

The run-information is an arbitrary information about the run which generates this message.

Please ensure that the working directory is the directory where the filechangedetectorConfig.json is. The startHash and
endHash are inclusive, so changes in these hashes will be reported. The program will put the log-file in a separate
logs-directory.

## Using the docker image

Make sure your sandbox is mounted to workingdirectory like

- `docker run --rm -a stdin -a stdout -u 0 -it -v C:\sandboxes\filechangedetector:/sandbox registry.gitlab.com/ppiag/filechangedetector:v1.3.5 "sh"`
- `cd sandbox`
  then run `java -jar /filechangedetector.jar branchname startHash endHash`
