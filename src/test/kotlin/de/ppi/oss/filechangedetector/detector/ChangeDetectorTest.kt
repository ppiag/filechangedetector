package de.ppi.oss.filechangedetector.detector

import de.ppi.oss.filechangedetector.config.ConfigLoader
import de.ppi.oss.filechangedetector.integration.EMail
import de.ppi.oss.filechangedetector.integration.Zulip
import io.mockk.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File


/**
 * Test for ChangeDetector. Inspired by
 * https://phauer.com/2018/best-practices-unit-testing-kotlin/.
 */
@Suppress("UsePropertyAccessSyntax")
class ChangeDetectorTest {

    private val commonStartHash = "9e1f4513307d7330a4b106cdd29898338fc62271"
    private val firstChangeHash = "9b0137e2af62061edacce08a354631434989b2b5"
    private val firstChangeHashPredecessor = "a075e122cb6612a45de9dd3c53c27d2fc8cd0ea6"

    private val listOfStartReferences = listOf("origin/testBranch", "origin/master")

    private val eMailMock: EMail = mockk()
    private val zulipMock: Zulip = mockk()

    private val eMailMessage = slot<String>()
    private val zulipMessage = slot<String>()

    private val testee = ChangeDetector(
        config = ConfigLoader.read(File("testConfig.json").readText(Charsets.UTF_8)),
        email = eMailMock, zulip = zulipMock, runInformation = "ChangeDetectorTest"
    )


    @BeforeEach
    fun init() {
        clearMocks(eMailMock, zulipMock)
        eMailMessage.clear()
        zulipMessage.clear()
        every {
            eMailMock.sendReport(any(), any(), capture(eMailMessage))
        } answers {
        }
        coEvery {
            zulipMock.sendReport(any(), any(), capture(zulipMessage))
        } coAnswers {
        }
    }

    @Test
    fun testChangesIn1WithStreamAndEmailChangeInEndHash() {
        emailAndZulipNotificationOfChange(
            commonStartHash,
            emptyList(),
            firstChangeHash,
            "9e1f4513307d7330a4b106cdd29898338fc62271"
        )
    }

    @Test
    fun testChangesIn1WithStreamAndEmailChangeInCommitAfterStartHash() {
        emailAndZulipNotificationOfChange(
            firstChangeHashPredecessor,
            emptyList(),
            "964fccd3dea1afae84e7203ccd03da0aa02a74d0",
            firstChangeHashPredecessor
        )
    }

    @Test
    fun testIllegalHashWithNonAnchestorStartHash() {
        emailAndZulipNotificationOfChange(
            "35c70529cb5d0c4ffc6efa29584a40fd10cac412",
            listOf("ABCDEFG", firstChangeHashPredecessor),
            "964fccd3dea1afae84e7203ccd03da0aa02a74d0",
            firstChangeHashPredecessor
        )
    }

    private fun emailAndZulipNotificationOfChange(
        possibleStartHash: String,
        startHash: List<String>,
        endHash: String,
        expectedStartHash: String
    ) {
        testee.searchForChanges(
            "testChangesIn1WithStreamAndEmail",
            possibleStartHash,
            startHash,
            endHash
        )
        val expectedContent = """
                        |Changes detected at branch **testChangesIn1WithStreamAndEmail** analyze from $expectedStartHash to $endHash in ChangeDetectorTest:
                        |
                        |niels: Initial Testfile-Set
                        |
                        |## Changes for testfiles/a/1.txt:
                        |```diff
                        |diff --git a/testfiles/a/1.txt b/testfiles/a/1.txt
                        |new file mode 100644
                        |index 0000000..cf04dd8
                        |--- /dev/null
                        |+++ b/testfiles/a/1.txt
                        |@@ -0,0 +1,5 @@
                        |+A file
                        |+in folder a
                        |+with name 1
                        |+-----
                        |+Some content
                        |\ No newline at end of file
                        |
                        |```
                        |
                        |### History of testfiles/a/1.txt:
                        |commit $firstChangeHash
                        |Author: niels <opensource21@gmail.com>
                        |Date:   Mon Aug 5 07:31:32 2019 +0200
                        |
                        |    Initial Testfile-Set
                        |
                        |""".trimMargin()
        Assertions.assertThat(eMailMessage.captured).isEqualTo(expectedContent)
        Assertions.assertThat(zulipMessage.captured).isEqualTo(expectedContent)
        coVerify(exactly = 1) {
            eMailMock.sendReport(listOf("test@example.com"), "testChangesIn1WithStreamAndEmail", any())
            zulipMock.sendReport("notifications", "testChangesIn1WithStreamAndEmail", any())
        }
    }

    @Test
    fun testChangesIn2FilesWithStream() {
        testee.searchForChanges(
            "testChangesIn2FilesWithStream",
            commonStartHash, emptyList(),
            firstChangeHash
        )
        coVerify(exactly = 1) {
            zulipMock.sendReport("notifications", "testChangesIn2FilesWithStream", any())
        }
        verify(exactly = 0) {
            eMailMock.sendReport(any(), any(), any())
        }
        val expectedContent = """
                    |Changes detected at branch **testChangesIn2FilesWithStream** analyze from 9e1f4513307d7330a4b106cdd29898338fc62271 to 9b0137e2af62061edacce08a354631434989b2b5 in ChangeDetectorTest:
                    |
                    |niels: Initial Testfile-Set
                    |
                    |## Changes for testfiles/a/1.txt:
                    |```diff
                    |diff --git a/testfiles/a/1.txt b/testfiles/a/1.txt
                    |new file mode 100644
                    |index 0000000..cf04dd8
                    |--- /dev/null
                    |+++ b/testfiles/a/1.txt
                    |@@ -0,0 +1,5 @@
                    |+A file
                    |+in folder a
                    |+with name 1
                    |+-----
                    |+Some content
                    |\ No newline at end of file
                    |
                    |```
                    |
                    |### History of testfiles/a/1.txt:
                    |commit $firstChangeHash
                    |Author: niels <opensource21@gmail.com>
                    |Date:   Mon Aug 5 07:31:32 2019 +0200
                    |
                    |    Initial Testfile-Set
                    |
                    |
                    |## Changes for testfiles/a/2.txt:
                    |```diff
                    |diff --git a/testfiles/a/2.txt b/testfiles/a/2.txt
                    |new file mode 100644
                    |index 0000000..c3e2172
                    |--- /dev/null
                    |+++ b/testfiles/a/2.txt
                    |@@ -0,0 +1,5 @@
                    |+A file
                    |+in folder a
                    |+with name 2
                    |+-----
                    |+A second content
                    |\ No newline at end of file
                    |
                    |```
                    |
                    |### History of testfiles/a/2.txt:
                    |commit $firstChangeHash
                    |Author: niels <opensource21@gmail.com>
                    |Date:   Mon Aug 5 07:31:32 2019 +0200
                    |
                    |    Initial Testfile-Set
                    |
                    |""".trimMargin()
        Assertions.assertThat(zulipMessage.captured).isEqualTo(expectedContent)
        Assertions.assertThat(eMailMessage.isCaptured).isFalse()

    }

    @Test
    fun testChangesInNonInteresistingBranch() {
        testee.searchForChanges("unkownBranch", commonStartHash, emptyList(), firstChangeHash)
        coVerify(exactly = 0) {
            eMailMock.sendReport(any(), any(), any())
            zulipMock.sendReport(any(), any(), any())
        }
    }


    @Test
    fun testChangesEmptyWithGuessStart() {
        testee.searchForChanges(
            "testChangesEmptyWithGuessStart",
            "00000000",
            listOfStartReferences,
            "87acb06d00d1c93886c8e38b26a03476a54958b6"
        )
        coVerify(exactly = 0) {
            eMailMock.sendReport(any(), any(), any())
            zulipMock.sendReport(any(), any(), any())
        }
    }

    @Test
    fun testChangesWithGuessStart() {
        testee.searchForChanges(
            "testChangesWithGuessStart",
            "00000000",
            listOf("origin/testBranch", "c709b79a4b674eeedcceb0eb63fa42b8b7ca4d71"),
            "87acb06d00d1c93886c8e38b26a03476a54958b6"
        )
        coVerify(exactly = 0) {
            eMailMock.sendReport(any(), any(), any())
        }
        coVerify(exactly = 1) {
            zulipMock.sendReport("notifications", "testChangesWithGuessStart", any())
        }
        val expectedContent = """
                    |Changes detected at branch **testChangesWithGuessStart** analyze from c709b79a4b674eeedcceb0eb63fa42b8b7ca4d71 to 87acb06d00d1c93886c8e38b26a03476a54958b6 in ChangeDetectorTest:
                    |
                    |niels: Start test
                    |
                    |## Changes for testfiles/b/2.txt:
                    |```diff
                    |diff --git a/testfiles/b/2.txt b/testfiles/b/2.txt
                    |index c81ab90..f4c421d 100644
                    |--- a/testfiles/b/2.txt
                    |+++ b/testfiles/b/2.txt
                    |@@ -2,4 +2,4 @@ A file
                    | in folder b
                    | with name 2
                    | -----
                    |-Some content
                    |\ No newline at end of file
                    |+Some content changed
                    |\ No newline at end of file
                    |
                    |```
                    |
                    |### History of testfiles/b/2.txt:
                    |commit 87acb06d00d1c93886c8e38b26a03476a54958b6
                    |Author: niels <opensource21@gmail.com>
                    |Date:   Tue Aug 20 12:38:52 2019 +0200
                    |
                    |    Start test
                    |
                    |""".trimMargin()
        Assertions.assertThat(zulipMessage.captured).isEqualTo(expectedContent)
        Assertions.assertThat(eMailMessage.isCaptured).isFalse()
    }

    @Test
    fun testRevertedChanges() {
        testee.searchForChanges(
            "testRevertedChanges",
            "bbdb489a3dd07149ee30cbc21b8bbf988e5508bb", emptyList(),
            "94bff6df64b9104bd8688c0677c3bf7380b31255"
        )
        coVerify(exactly = 0) {
            eMailMock.sendReport(any(), any(), any())
            zulipMock.sendReport(any(), any(), any())
        }
    }

    @Test
    fun testWrongBranchPatterns() {
        val worngPatternTestee = ChangeDetector(
            config = ConfigLoader.read(File("testConfigWrongBranches.json").readText(Charsets.UTF_8)),
            email = eMailMock, zulip = zulipMock, runInformation = "ChangeDetectorTest"
        )
        worngPatternTestee.searchForChanges(
            "testWrongBranchPatterns",
            "00000000",
            listOf("origin/testBranch", "c709b79a4b674eeedcceb0eb63fa42b8b7ca4d71"),
            "87acb06d00d1c93886c8e38b26a03476a54958b6"
        )
        val exectedBranchMessage =
            """Illegal Branch-Pattern Syntax '*' for subject testWrongBranchPatterns: Dangling meta character '*' near index 0
               |*
               |^.""".trimMargin()
        coVerify(exactly = 1) {
            eMailMock.sendReport(listOf("test@example.com"), "Wrong branch-pattern", exectedBranchMessage)
            zulipMock.sendReport("notification", "Wrong branch-pattern", exectedBranchMessage)
        }
        coVerify(exactly = 1) {
            eMailMock.sendReport(listOf("test@example.com"), "testWrongBranchPatterns", any())
            zulipMock.sendReport("notification", "testWrongBranchPatterns", any())
        }
        val warnMessage = "**Illegal File-Pattern Syntax '*' for subject testWrongBranchPatterns.**\n##"
        Assertions.assertThat(zulipMessage.captured).contains(warnMessage)
        Assertions.assertThat(eMailMessage.captured).contains(warnMessage)
    }
}