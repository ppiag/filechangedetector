package de.ppi.oss.filechangedetector.integration

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.ppi.oss.filechangedetector.config.ZulipServer
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assumptions
import org.junit.jupiter.api.Test

import java.io.File

/**
 * This test is for Testing the Implementation in real-world. So normally it's ignored, but it's maken it easier to test
 * in a specific environment
 */
class IntegrationTest {

    @Test
    fun sendReportViaEMail() {
        val configFile = File("secrets/emailServerConfig.json")
        Assumptions.assumeTrue(configFile.exists(), "${configFile.absolutePath} doesn't exists")
        val objectMapper = jacksonObjectMapper()
        val email: EMail = EMailImpl(objectMapper.readValue(configFile))
        email.sendReport(listOf("test@example.com"), "Test", """
            # This is a Test-Email  
            Please *ignore*.""")
    }

    @Test
    fun sendReportViaZulip() = runBlocking {
        val configFile = File("secrets/zulipServerConfig.json")
        Assumptions.assumeTrue(configFile.exists(), "${configFile.absolutePath} doesn't exists")
        val objectMapper = jacksonObjectMapper()
        val zulipServer = objectMapper.readValue<ZulipServer>(configFile)
        val email: Zulip = ZulipImpl(zulipServer)
        email.sendReport("zulip/spielkiste", "Test-Report", "Please ignore.")
    }

}