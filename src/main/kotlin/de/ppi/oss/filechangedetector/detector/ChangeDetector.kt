package de.ppi.oss.filechangedetector.detector

import de.ppi.oss.filechangedetector.config.DiffDetectorConfig
import de.ppi.oss.filechangedetector.integration.EMail
import de.ppi.oss.filechangedetector.integration.Zulip
import de.ppi.oss.kzulip.api.messages.maxContentSize
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import org.apache.logging.log4j.LogManager
import java.util.regex.PatternSyntaxException

/**
 * The class which do the work and search for changes.
 */
class ChangeDetector(
    private val config: DiffDetectorConfig,
    private val email: EMail,
    private val zulip: Zulip,
    private val runInformation: String
) {
    private val logger = LogManager.getLogger(ChangeDetector::class.java)
    private val git = GitNative(config.gitRepo, config.gitCommand)

    fun searchForChanges(
        branchname: String,
        possibleStartHash: String,
        startReferences: List<String>,
        endHash: String
    ) = runBlocking(CoroutineName("main")) {
        val startHash = if (git.isValidCommitHash(possibleStartHash) && git.isAnchestorOf(possibleStartHash, endHash)) {
            possibleStartHash
        } else {
            findStartHash(startReferences, endHash)
        }
        val logMessage = "Analyze $branchname from $startHash to $endHash"
        println(logMessage)
        logger.info(logMessage)
        val fileList = Channel<String>(10)
        launch { git.getFileList(startHash, endHash, fileList) }
        val relevantObserverConfig = config.observerConfigs.filter { config ->
            config.branchPatterns.map {
                try {
                    Regex(it)
                } catch (e: PatternSyntaxException) {
                    val message =
                        "Illegal Branch-Pattern Syntax '$it' for subject ${config.subject}: ${e.localizedMessage}."
                    val subject = "Wrong branch-pattern"
                    if (!config.email.isNullOrEmpty()) {
                        email.sendReport(config.email, subject, message)
                    }
                    if (config.stream != null) {
                        zulip.sendReport(config.stream, subject, message)
                    }
                    logger.error(message, e)
                    Regex(".*")
                }
            }.any { it.matches(branchname) }
        }

        val changeAnalyzers = relevantObserverConfig.map { ChangeAnalyzer(startHash, endHash, git, it) }

        launch { dispatch(fileList, changeAnalyzers.map { it.filenameChannel }) }

        withContext(Dispatchers.Default + CoroutineName("consume")) {
            for (changeAnalyzer in changeAnalyzers) {
                launch {
                    changeAnalyzer.analyze()
                }
            }
        }
        createAndSendReport(changeAnalyzers, branchname, startHash, endHash)
    }

    private suspend fun createAndSendReport(
        changeAnalyzers: List<ChangeAnalyzer>,
        branchname: String,
        startHash: String,
        endHash: String
    ) {
        for (changeAnalyzer in changeAnalyzers.filter { it.existReport() }) {
            val reportList = changeAnalyzer.getReport()
            val header =
                "Changes detected at branch **$branchname** analyze from $startHash to $endHash in $runInformation:"
            val overview = changeAnalyzer.getGitLogOverview()
            val report = "$header\n\n$overview\n${reportList.joinToString("\n")}"
            logger.debug("${changeAnalyzer.config.subject}: $report")
            if (!changeAnalyzer.config.email.isNullOrEmpty()) {
                email.sendReport(changeAnalyzer.config.email, changeAnalyzer.config.subject, report)
            }
            if (changeAnalyzer.config.stream != null) {
                var zulipReport = StringBuilder("$header\n\n$overview")
                for (reportPart in reportList) {
                    if (zulipReport.length + reportPart.length > maxContentSize) {
                        zulip.sendReport(
                            changeAnalyzer.config.stream,
                            changeAnalyzer.config.subject.take(60),
                            zulipReport.toString()
                        )
                        zulipReport = StringBuilder(header)
                    }
                    zulipReport = zulipReport.append("\n").append(reportPart)
                }
                zulip.sendReport(
                    changeAnalyzer.config.stream,
                    changeAnalyzer.config.subject.take(60),
                    zulipReport.toString()
                )
            }
        }
    }

    /**
     * Find the most recent mergebase.
     */
    private suspend fun findStartHash(startReferences: List<String>, endHash: String): String {
        val listOfMergeBaseHashes = startReferences.pmap { mergeBaseOrNull(it, endHash) }.filterNotNull()
        return listOfMergeBaseHashes.sortedWith { hash1, hash2 ->
            when {
                hash1 == hash2 -> 0
                git.isAnchestorOf(hash1, hash2) -> -1
                else -> 1
            }
        }.last()
    }

    /**
     * Runs map in parallel.
     * @see Iterable.map
     */
    private suspend fun <A, B> Iterable<A>.pmap(f: suspend (A) -> B?): List<B?> = coroutineScope {
        map { async { f(it) } }.map { it.await() }
    }

    private fun mergeBaseOrNull(hashA: String, hashB: String): String? {
        try {
            return git.getMergeBase(hashA, hashB)
        } catch (e: IllegalStateException) {
            logger.info("Error finding merge-base - skip $hashA.", e)
        }
        return null
    }

    private suspend fun dispatch(input: ReceiveChannel<String>, workerInput: List<SendChannel<String>>) {
        for (fileName in input) {
            logger.debug("Send $fileName to ${workerInput.size} analyzer.")
            for (worker in workerInput) {
                worker.send(fileName)
            }
        }
        for (worker in workerInput) {
            worker.close()
        }
    }
}
