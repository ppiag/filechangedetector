package de.ppi.oss.filechangedetector.detector

import de.ppi.oss.filechangedetector.config.ObserverConfig
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.withContext
import org.apache.logging.log4j.LogManager
import java.util.regex.PatternSyntaxException


/**
 * Analyze the file and creates a report for matching files.
 */
class ChangeAnalyzer(
    private val startHash: String,
    private val endHash: String,
    private val git: GitNative,
    val config: ObserverConfig
) {
    private val logger = LogManager.getLogger(ChangeAnalyzer::class.java)

    private val reports = mutableListOf<Deferred<String>>()
    private val relevantFiles = mutableListOf<String>()
    private var extraMessage = ""
    private val fileRegexp = config.filePatterns.map { pattern ->
        try {
            Regex(pattern)
        } catch (e: PatternSyntaxException) {
            extraMessage = "**Illegal File-Pattern Syntax '$pattern' for subject ${config.subject}.**\n"
            logger.error(extraMessage, e)
            Regex(".*")
        }
    }

    val filenameChannel = Channel<String>()

    fun existReport(): Boolean {
        return reports.size > 0
    }


    fun getGitLogOverview(): String {
        return git.getFileLogOneline(startHash, endHash, relevantFiles.toList())
    }

    suspend fun getReport(): List<String> {
        return reports.map { it.await() }
    }

    suspend fun analyze() {
        for (fileName in filenameChannel) {
            analyze(fileName)
        }

    }

    private suspend fun analyze(fileName: String) {
        if (this.fileRegexp.any { regex -> regex.matches(fileName) }) {
            relevantFiles.add(fileName)
            withContext(Dispatchers.IO) {
                reports.add(async {
                    createChangeReport(fileName)
                })
            }
        }
    }

    private fun createChangeReport(fileName: String): String {
        logger.debug("Create report for $fileName.")
        val fileDiff = git.getFileDiff(startHash, endHash, fileName)
        val fileLog = git.getFileLog(startHash, endHash, fileName)
        return """$extraMessage## Changes for $fileName:
                 |```diff
                 |$fileDiff
                 |```
                 |
                 |### History of $fileName:
                 |$fileLog
                 |""".trimMargin()
    }
}