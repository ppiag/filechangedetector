package de.ppi.oss.filechangedetector.detector

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.LogManager
import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * Native implementation of java-calls. JGit seems to be to complicated.
 *
 * - `git diff --name-only 66e8f8963ac7f6997c618fccb3ed7c156452eed2..799aa67adcd25dd06e5baa6ce95b2d41e57d4960`
 * - `git diff 66e8f8963ac7f6997c618fccb3ed7c156452eed2..799aa67adcd25dd06e5baa6ce95b2d41e57d4960 -- ph-ws-svc/src/test/java/de/ppi/tph/ws/svc/impl/qa/WebservicesServicePackageDependencyTest.java`
 * - `git log --follow 66e8f8963ac7f6997c618fccb3ed7c156452eed2..799aa67adcd25dd06e5baa6ce95b2d41e57d4960 -- ph-ws-svc/src/test/java/de/ppi/tph/ws/svc/impl/qa/WebservicesServicePackageDependencyTest.java`
 *
 * man kann auch
 * git log -U -m  66e8f8963ac7f6997c618fccb3ed7c156452eed2..799aa67adcd25dd06e5baa6ce95b2d41e57d4960 nehmen:
 * Jeder Commit startet mit commit und jedes diff mit diff. Aber ich glaube git ist da schneller und man muss unendlich viel output wegwerfen.
 * withContext(Dispatchers.IO) {
 * git operationen.
 * }
 * https://medium.com/@elizarov/blocking-threads-suspending-coroutines-d33e11bf4761
 */
class GitNative(private val repoPath: String, private val gitCommand: String) {

    private val gitRepo = {
        val gitRepo = File(repoPath)
        if (!gitRepo.exists()) {
            throw IllegalStateException("${gitRepo.absolutePath} does not exists")
        }
        gitRepo
    }.invoke()

    private val logger = LogManager.getLogger(GitNative::class.java)

    /**
     * Checks if a given sha-hash is invalid, if not a warn is logged.
     */
    fun isValidCommitHash(shaHash: String): Boolean {
        val (_, _: String, exitCode) = runGitCommandInternal(listOf(gitCommand, "cat-file", "-e", "$shaHash^{commit}"))
        if (exitCode != 0) {
            logger.warn("$shaHash is invalid.")
        }
        return (exitCode == 0)
    }

    fun getFileList(startHash: String, endHash: String, changedFiles: SendChannel<String>) {
        runGitCommandStream(listOf(gitCommand, "diff", "--name-only", "$startHash..$endHash"), changedFiles)
    }

    fun getFileDiff(startHash: String, endHash: String, fileName: String): String {
        return runGitCommand(listOf(gitCommand, "diff", "$startHash..$endHash", "--", fileName))
    }

    fun getFileLog(startHash: String, endHash: String, fileName: String): String {
        return runGitCommand(listOf(gitCommand, "log", "--follow", "$startHash..$endHash", "--", fileName))
    }

    fun getFileLogOneline(startHash: String, endHash: String, fileNames: List<String>): String {
        val commandList = mutableListOf(gitCommand, "log", "--format=format:%cn: %s%n", "$startHash..$endHash", "--")
        commandList.addAll(fileNames)
        return runGitCommand(commandList.toList())
    }

    fun getMergeBase(firstReference: String, secondReference: String): String {
        return runGitCommand(listOf(gitCommand, "merge-base", firstReference, secondReference)).trim()
    }

    /**
     * Checks if the anchestor-commit-reference is anchestor of the given refernence.
     */
    fun isAnchestorOf(anchestorReference: String, reference: String): Boolean {
        val commandArguments = listOf(gitCommand, "merge-base", "--is-ancestor", anchestorReference, reference)
        val (stdout, stderr, exitCode) = runGitCommandInternal(commandArguments)
        if (exitCode > 1 || exitCode < 0) {
            throw IllegalStateException(
                "In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< " +
                        "failed with exitCode $exitCode and output >$stdout< or error >$stderr<."
            )
        }
        return exitCode == 0
    }

    private fun runGitCommandStream(commandArguments: List<String>, sendChannel: SendChannel<String>) {
        try {
            val proc = ProcessBuilder(*commandArguments.toTypedArray())
                .directory(gitRepo)
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.PIPE)
                .start()
            val output = proc.inputStream.bufferedReader().lines()
                .forEach { text: String -> runBlocking(Dispatchers.Default) { sendChannel.send(text) } }
            val error = proc.errorStream.bufferedReader().readText()
            if (!proc.waitFor(1L, TimeUnit.MINUTES)) {
                throw TimeoutException(
                    "In directory ${gitRepo.absolutePath} command " +
                            ">${commandArguments.joinToString(" ")}< failed with exitCode ${proc.exitValue()} " +
                            "and output >$output< or error >$error<."
                )
            }
            sendChannel.close()
            if (proc.exitValue() != 0) {
                throw IllegalStateException(
                    "In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< " +
                            "failed with exitCode ${proc.exitValue()} and output >$output< or error >$error<."
                )
            }
            logger.debug("In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}<")
        } catch (e: IOException) {
            throw IllegalStateException(
                "Command >${commandArguments.joinToString(" ")}< failed with Exception ${e.message}",
                e
            )
        }
    }

    private fun runGitCommand(commandArguments: List<String>): String {
        val (stdout, stderr: String, exitCode) = runGitCommandInternal(commandArguments)
        if (exitCode != 0) {
            throw IllegalStateException(
                "In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< " +
                        "failed with exitCode $exitCode and output >$stdout< or error >$stderr<."
            )
        }
        return stdout

    }

    private fun runGitCommandInternal(commandArguments: List<String>): Triple<String, String, Int> {
        try {
            val proc = ProcessBuilder(*commandArguments.toTypedArray())
                .directory(gitRepo)
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.PIPE)
                .start()
            val output = proc.inputStream.bufferedReader().readText()
            val error = proc.errorStream.bufferedReader().readText()
            logger.debug("In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< produced following output\n$output")
            if (!proc.waitFor(1L, TimeUnit.MINUTES)) {
                throw TimeoutException("In directory ${gitRepo.absolutePath} command >${commandArguments.joinToString(" ")}< failed with exitCode ${proc.exitValue()} and output >$output< or error >$error<.")
            }
            return Triple(output, error, proc.exitValue())
        } catch (e: IOException) {
            throw IllegalStateException(
                "Command >${commandArguments.joinToString(" ")}< failed with Exception ${e.message}",
                e
            )
        }
    }
}
