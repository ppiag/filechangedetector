package de.ppi.oss.filechangedetector

import de.ppi.oss.filechangedetector.config.ConfigLoader
import de.ppi.oss.filechangedetector.detector.ChangeDetector
import de.ppi.oss.filechangedetector.integration.*
import kotlinx.coroutines.DEBUG_PROPERTY_NAME
import kotlinx.coroutines.DEBUG_PROPERTY_VALUE_ON
import kotlin.system.exitProcess

/**
 * The main starter application.
 */
class Application {
    object Companion {
        @JvmStatic
        fun main(args: Array<String>) {
            System.setProperty(DEBUG_PROPERTY_NAME, DEBUG_PROPERTY_VALUE_ON)
            if (args.size < 5) {
                System.err.println("Usage: Application <Run-Information> <Branchname> <startHash exclusive or 00000000> <endHash inclusive>  <list of possible start branches>")
                exitProcess(1)
            }
            val config = ConfigLoader.read()
            val email: EMail = if (config.emailServer != null) {
                EMailImpl(config.emailServer)
            } else {
                EMailLog()
            }
            val zulip: Zulip = if (config.zulipServer != null) {
                ZulipImpl(config.zulipServer)
            } else {
                ZulipLog()
            }
            ChangeDetector(config, email, zulip, args[0]).searchForChanges(args[1], args[2], args.drop(4), args[3])
        }
    }
}