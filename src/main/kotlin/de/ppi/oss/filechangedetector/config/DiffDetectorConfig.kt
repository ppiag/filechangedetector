package de.ppi.oss.filechangedetector.config

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.ppi.oss.filechangedetector.Application
import de.ppi.oss.kzulip.client.HttpLogLevel
import org.apache.logging.log4j.LogManager
import java.io.File
import kotlin.system.exitProcess

/**
 * Loader for the config. Normally from diffConfig.json in working directory or parallel to the jar.
 */
object ConfigLoader {
    private val logger = LogManager.getLogger(ConfigLoader::class.java)

    fun read(): DiffDetectorConfig {
        val propertiesFileName = "filechangedetectorConfig.json"
        var configFile = File(propertiesFileName)
        if (!configFile.exists()) {
            val loadClass = try {
                // First try if is a Sprint-Boot-Runner-jar.
                ClassLoader.getSystemClassLoader().loadClass("org.springframework.boot.loader.Launcher")
            } catch (e: ClassNotFoundException) {
                Application::class.java
            }
            val location = loadClass.protectionDomain.codeSource.location
            val currentLocation = File(location.toURI())
            configFile = if (currentLocation.isDirectory) {
                File(currentLocation, propertiesFileName)
            } else {
                File(currentLocation.parent, propertiesFileName)
            }
        }

        if (configFile.exists()) {
            logger.info("Load properties from : ${configFile.absolutePath}")
        } else {
            logger.error("Can't load properties from : ${configFile.absolutePath}")
            exitProcess(1)
        }
        return read(configFile.readText(Charsets.UTF_8))
    }

    fun read(configFile: String): DiffDetectorConfig {
        val objectMapper = jacksonObjectMapper()
        return objectMapper.readValue(configFile)
    }
}

/**
 * The root of configuration.
 */
data class DiffDetectorConfig(
    val emailServer: EmailServer?,
    val zulipServer: ZulipServer?,
    val gitRepo: String,
    val gitCommand: String = "git",
    val observerConfigs: List<ObserverConfig>
)

data class EmailServer(
    val server: String,
    val port: Int,
    val user: String,
    val password: String,
    val senderEmail: String
)

data class ZulipServer(
    val site: String,
    val email: String,
    val apiKey: String,
    val httpLogLevel: HttpLogLevel = HttpLogLevel.INFO
)

data class ObserverConfig(
    val subject: String,
    val email: List<String>?,
    val stream: String?,
    val branchPatterns: List<String>,
    val filePatterns: List<String>
)
