package de.ppi.oss.filechangedetector.integration

import de.ppi.oss.filechangedetector.config.ZulipServer
import de.ppi.oss.kzulip.api.messages.SendMessageRequest
import de.ppi.oss.kzulip.client.ZulipClient
import org.apache.logging.log4j.LogManager

interface Zulip {
    /**
     * Send a report to the given Stream.
     * @param stream the name of the stream
     * @param subject a subject with a maximal lenght of 60 characters
     * @param content a message with a maximal size of 10000 bytes
     */
    suspend fun sendReport(stream: String, subject: String, content: String)
}

/**
 * Logging implementation, which only log that something should be send.
 */
class ZulipLog : Zulip {
    private val logger = LogManager.getLogger(ZulipLog::class.java)
    override suspend fun sendReport(stream: String, subject: String, content: String) {
        logger.warn(
            "No E-Zulip-Server configured, drop Message for $stream " +
                    "with subject $subject\n and content $content"
        )
    }
}

class ZulipImpl(server: ZulipServer) : Zulip {
    private val logger = LogManager.getLogger(ZulipImpl::class.java)
    private val client = ZulipClient(server.site, server.email, server.apiKey, server.httpLogLevel)

    override suspend fun sendReport(stream: String, subject: String, content: String) {
        logger.info("Send report to stream '$stream' with subject '$subject")
        client.sendMessage(SendMessageRequest(stream, subject, content))
    }

}