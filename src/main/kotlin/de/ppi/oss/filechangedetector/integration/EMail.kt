package de.ppi.oss.filechangedetector.integration

import de.ppi.oss.filechangedetector.config.EmailServer
import org.apache.logging.log4j.LogManager
import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


interface EMail {
    fun sendReport(recipients: List<String>, subject: String, content: String)
}

/**
 * Logging implementation, which only log that something should be send.
 */
class EMailLog : EMail {
    private val logger = LogManager.getLogger(EMailLog::class.java)
    override fun sendReport(recipients: List<String>, subject: String, content: String) {
        logger.warn(
            "No E-Mail-Server configured, drop E-Mail for ${recipients.joinToString(",")} " +
                    "with subject $subject\n and content $content"
        )
    }
}

class EMailImpl(private val server: EmailServer) : EMail {
    private val logger = LogManager.getLogger(EMailImpl::class.java)
    override fun sendReport(recipients: List<String>, subject: String, content: String) {
        val auth = MailAuthenticator(server.user, server.password)
        val properties = Properties()
        // set email properties
        properties["mail.smtp.host"] = server.server
        properties["mail.smtp.auth"] = "true"
        properties["mail.smtp.port"] = server.port.toString()

        // create email session
        val session = Session.getDefaultInstance(properties, auth)

        // create new email
        val msg = MimeMessage(session)

        // set addresses
        msg.setFrom(InternetAddress(server.senderEmail))
        val recipientsAddress = recipients.joinToString(",")
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientsAddress, false))

        // set subject and body
        msg.subject = subject
        msg.setContent(createHtmlText(content), "text/html; charset=utf-8")

        // set additional header information
        msg.setHeader("FileChangeDetector", "FileChangeDetector generated email")
        msg.sentDate = Date()

        // Send email
        logger.debug("Email send to $recipientsAddress subject: $subject")
        Transport.send(msg)
    }

    private fun createHtmlText(content: String): String {
        val parser = Parser.builder().build()
        val document = parser.parse(content)
        val renderer = HtmlRenderer.builder().build()
        return renderer.render(document)
    }


    private inner class MailAuthenticator(private val user: String, private val password: String) : Authenticator() {
        override fun getPasswordAuthentication(): PasswordAuthentication {
            return PasswordAuthentication(this.user, this.password)
        }
    }
}

