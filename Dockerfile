FROM  adoptopenjdk/openjdk8:x86_64-alpine-jdk8u282-b08-slim
RUN apk add --update git openssh-client && rm -rf /var/cache/apk/*
COPY target/filechangedetector*spring-boot.jar filechangedetector.jar
